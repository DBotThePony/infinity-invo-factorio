
-- Copyright (C) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local script = script
local defines = defines
local math = math

local function OnTick(event)
	local to_tick = storage.to_tick
	if not to_tick then return end
	local tick = event.tick

	local index, next_tick = next(to_tick)

	if not index then
		storage.to_tick = nil
		return
	end

	local main = defines.inventory.character_main

	while index do
		if next_tick <= tick then
			do
				local get_player = game.players[index]
				if not get_player then goto CONTINUE end

				local controller_type = get_player.controller_type
				if controller_type ~= defines.controllers.character then goto CONTINUE end
				if not get_player.character then goto CONTINUE end

				-- avoid resizing when player is somehow look at their own GUI
				-- unless it is emergency
				local avoid = get_player.opened_self

				if not avoid then
					local opened_gui_type = get_player.opened_gui_type

					avoid = opened_gui_type == defines.gui_type.entity or
						opened_gui_type == defines.gui_type.other_player or
						opened_gui_type == defines.gui_type.item or
						opened_gui_type == defines.gui_type.player_management
				end

				-- you can't resize inventories created by the game engine logic
				-- sad
				local inventory = get_player.get_inventory(main)
				if not inventory then goto CONTINUE end

				if inventory.is_empty() then
					get_player.character_inventory_slots_bonus = 0
					goto CONTINUE
				end

				local empty = 0
				local slots = #inventory
				local increase = math.min(math.max(1, math.floor(slots / 50)) * 10, 100)

				if avoid then
					local thersold = math.min(math.max(4, math.floor(slots) / 20), 20)

					for i = slots, 1, -1 do
						local stack = inventory[i]

						if not stack or stack.count == 0 then
							empty = empty + 1

							if empty > thersold then goto CONTINUE end
						end
					end

					get_player.character_inventory_slots_bonus = math.min(65000, get_player.character_inventory_slots_bonus + increase)
				else
					local thersold = increase * 2

					for i = slots, 1, -1 do
						local stack = inventory[i]

						if not stack or stack.count == 0 then
							empty = empty + 1

							-- if empty > 10 then return end
							if empty > thersold then break end
						end
					end

					if empty > thersold then
						get_player.character_inventory_slots_bonus = math.max(0, get_player.character_inventory_slots_bonus - increase)
					elseif empty < increase then
						get_player.character_inventory_slots_bonus = math.min(65000, get_player.character_inventory_slots_bonus + increase)
					end
				end
			end

			::CONTINUE::
			local _index = index
			index, next_tick = next(to_tick, index)
			to_tick[_index] = nil
		else
			index, next_tick = next(to_tick, index)
		end
	end
end

local function UpdatePlayer(event)
	if not storage.to_tick then storage.to_tick = {} end
	storage.to_tick[event.player_index] = event.tick + 60
end

script.on_event(defines.events.on_tick, OnTick)
script.on_event(defines.events.on_player_main_inventory_changed, UpdatePlayer)
